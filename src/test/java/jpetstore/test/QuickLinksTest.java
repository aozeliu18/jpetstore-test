package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class QuickLinksTest extends BaseTest {

    @Test
    public void testQuickLinks() {
        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to Fish category
        $(By.cssSelector("#QuickLinks a:nth-of-type(1)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Fish"));

        // Move to Dogs category
        $(By.cssSelector("#QuickLinks a:nth-of-type(2)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Dogs"));

        // Move to Reptiles category
        $(By.cssSelector("#QuickLinks a:nth-of-type(3)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Reptiles"));

        // Move to Cats category
        $(By.cssSelector("#QuickLinks a:nth-of-type(4)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Cats"));

        // Move to Birds category
        $(By.cssSelector("#QuickLinks a:nth-of-type(5)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Birds"));
    }
}
