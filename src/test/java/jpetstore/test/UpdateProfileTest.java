package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class UpdateProfileTest extends BaseTest {

    @Test
    public void testUpdateProfile() {
        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to sign in page & sign
        $(By.linkText("Sign In")).click();
        $(By.name("username")).setValue("j2ee");
        $(By.name("password")).setValue("j2ee");
        $(By.name("signon")).click();
        $(By.id("WelcomeContent")).shouldBe(text("Welcome ABC!"));

        // Show profile page
        $(By.linkText("My Account")).click();
        $(By.cssSelector("#Catalog h3")).shouldBe(text("User Information"));
        $$(By.cssSelector("#Catalog table td")).get(1).shouldBe(text("j2ee"));

        // Edit account
        $(By.name("account.phone")).setValue("555-555-5556");
        $(By.name("editAccount")).click();
        $(By.cssSelector("#Catalog h3")).shouldBe(text("User Information"));
        $$(By.cssSelector("#Catalog table td")).get(1).shouldBe(text("j2ee"));
        $(By.name("account.phone")).shouldBe(value("555-555-5556"));
    }
}
