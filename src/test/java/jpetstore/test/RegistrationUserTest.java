package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class RegistrationUserTest extends BaseTest {

    @Test
    public void testRegistrationUser() {
        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to sign in page & sign
        $(By.linkText("Sign In")).click();
        $(By.cssSelector("#Catalog p")).shouldBe(text("Please enter your username and password."));

        // Move to use registration page
        $(By.linkText("Register Now!")).click();
        $(By.cssSelector("#Catalog h3")).shouldBe(text("User Information"));

        // Create a new user
        String userId = String.valueOf(System.currentTimeMillis());
        $(By.name("username")).setValue(userId);
        $(By.name("password")).setValue("password");
        $(By.name("repeatedPassword")).setValue("password");
        $(By.name("account.firstName")).setValue("Jon");
        $(By.name("account.lastName")).setValue("MyBatis");
        $(By.name("account.email")).setValue("jon.mybatis@test.com");
        $(By.name("account.phone")).setValue("09012345678");
        $(By.name("account.address1")).setValue("Address1");
        $(By.name("account.address2")).setValue("Address2");
        $(By.name("account.city")).setValue("Minato-Ku");
        $(By.name("account.state")).setValue("Tokyo");
        $(By.name("account.zip")).setValue("0001234");
        $(By.name("account.country")).setValue("Japan");
        $(By.name("account.languagePreference")).selectOption("japanese");
        $(By.name("account.favouriteCategoryId")).selectOption("CATS");
        $(By.name("account.listOption")).setSelected(true);
        $(By.name("account.bannerOption")).setSelected(true);
        $(By.name("newAccount")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to sign in page & sign
        $(By.linkText("Sign In")).click();
        $(By.name("username")).setValue(userId);
        $(By.name("password")).setValue("password");
        $(By.name("signon")).click();
        $(By.id("WelcomeContent")).shouldBe(text("Welcome Jon!"));

    }
}
