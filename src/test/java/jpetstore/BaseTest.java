package jpetstore;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Browsers.HTMLUNIT;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.$;

public class BaseTest {

    @BeforeEach
    public void setupSelenide() {
        htmlunit();
        timeout = TimeUnit.SECONDS.toMillis(10);
        baseUrl = "http://0.0.0.0:8000/jpetstore";
    }

    private void htmlunit(){
        browser = HTMLUNIT;
        WebDriver webDriver = WebDriverRunner.getAndCheckWebDriver();
        try {
            Field webClientField = HtmlUnitDriver.class.getDeclaredField("webClient");
            webClientField.setAccessible(true);
            WebClient webClient = (WebClient) webClientField.get(webDriver);
            webClient.addRequestHeader("job_id", System.getenv().getOrDefault("TASK_ID", "anonymous-task"));
            webClient.addRequestHeader("spec_file_name", this.getClass().getSimpleName());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @AfterEach
    public void logout() {
        SelenideElement element = $(By.linkText("Sign Out"));
        if (element.exists()) {
            element.click();
        }
        WebDriverRunner.getWebDriver().close();
    }
}
