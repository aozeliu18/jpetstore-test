package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class ViewHelpTest extends BaseTest {

    @Test
    public void testViewHelp() {
        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to help
        $(By.linkText("?")).click();
        $(By.cssSelector("#Content h1")).shouldBe(text("JPetStore Demo"));
    }
}
