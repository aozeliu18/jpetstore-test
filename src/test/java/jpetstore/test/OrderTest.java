package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class OrderTest extends BaseTest {

    @Test
    public void testOrder() {

        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");
        $(By.cssSelector("#Content h2")).shouldBe(text("Welcome to JPetStore 6"));

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to sign in page & sign
        $(By.linkText("Sign In")).click();
        $(By.name("username")).setValue("j2ee");
        $(By.name("password")).setValue("j2ee");
        $(By.name("signon")).click();
        $(By.id("WelcomeContent")).shouldBe(text("Welcome ABC!"));

        // Search items
        $(By.name("keyword")).setValue("fish");
        $(By.name("searchProducts")).click();
        $$(By.cssSelector("#Catalog table tr")).shouldHaveSize(4);

        // Select item
        $(By.linkText("Fresh Water fish from China")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Goldfish"));

        // Add a item to the cart
        $(By.linkText("Add to Cart")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Shopping Cart"));

        // Add a item to the cart
        $(By.cssSelector("#QuickLinks a:nth-of-type(5)")).click();
        $(By.linkText("AV-CB-01")).click();
        $(By.linkText("EST-18")).click();
        $(By.linkText("Add to Cart")).click();
        $(By.cssSelector("#Cart tr:nth-of-type(4) td")).shouldBe(text("Sub Total: $199.00"));

        // Update quantity
        $(By.name("EST-20")).setValue("10");
        $(By.name("updateCartQuantities")).click();
        $(By.cssSelector("#Catalog tr td:nth-of-type(7)")).shouldBe(text("$55.00"));
        $(By.cssSelector("#Cart tr:nth-of-type(4) td")).shouldBe(text("Sub Total: $248.50"));

        // Remove item
        $(By.cssSelector("#Cart tr:nth-of-type(2) td:nth-of-type(8) a")).click();
        $(By.cssSelector("#Cart tr:nth-of-type(3) td")).shouldBe(text("Sub Total: $55.00"));

        // Checkout cart items
        $(By.linkText("Proceed to Checkout")).click();
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Changing shipping address
        $(By.name("shippingAddressRequired")).click();
        $(By.name("newOrder")).click();
        $(By.cssSelector("#Catalog tr th")).shouldBe(text("Shipping Address"));
        $(By.name("order.shipAddress2")).setValue("MS UCUP02-207");

        // Confirm order information
        $(By.name("newOrder")).click();
        $(By.cssSelector("#Catalog")).shouldBe(text("Please confirm the information below and then press continue..."));

        // Submit order
        $(By.linkText("Confirm")).click();
        $(By.cssSelector(".messages li")).shouldBe(text("Thank you, your order has been submitted."));
        String orderId = extractOrderId($(By.cssSelector("#Catalog table tr")).text());

        // Show profile page
        $(By.linkText("My Account")).click();
        $(By.cssSelector("#Catalog h3")).shouldBe(text("User Information"));

        // Show orders
        $(By.linkText("My Orders")).click();
        $(By.cssSelector("#Content h2")).shouldBe(text("My Orders"));

        // Show order detail
        $(By.linkText(orderId)).click();
        assertThat(extractOrderId($(By.cssSelector("#Catalog table tr")).text())).isEqualTo(orderId);

        // Sign out
        $(By.linkText("Sign Out")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

    }

    private static String extractOrderId(String target) {
        Matcher matcher = Pattern.compile("Order #(\\d{4}) .*").matcher(target);
        String orderId = "";
        if (matcher.find()) {
            orderId = matcher.group(1);
        }
        return orderId;
    }

}
