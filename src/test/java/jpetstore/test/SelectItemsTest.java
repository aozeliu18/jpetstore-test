package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class SelectItemsTest extends BaseTest {

    @Test
    public void testSelectItems() {
        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to category
        $(By.cssSelector("#SidebarContent a")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Fish"));

        // Move to items
        $(By.linkText("FI-SW-01")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Angelfish"));

        // Move to item detail
        $(By.linkText("EST-1")).click();
        $$(By.cssSelector("#Catalog table tr td")).get(2).shouldBe(text("Large Angelfish"));

        // Back to items
        $(By.linkText("Return to FI-SW-01")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Angelfish"));

        // Back to category
        $(By.linkText("Return to FISH")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Fish"));

        // Back to the top page
        $(By.linkText("Return to Main Menu")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

    }
}
