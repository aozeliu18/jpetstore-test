package jpetstore.test;

import com.codeborne.selenide.junit5.ScreenShooterExtension;
import jpetstore.BaseTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for screen transition.
 *
 * @author Kazuki Shimizu
 */
@ExtendWith(ScreenShooterExtension.class)
public class MainImageContentOnTopPageTest extends BaseTest {

    @Test
    public void testMainImageContentOnTopPage() {
        // Open the home page
        open("/");
        assertThat(title()).isEqualTo("JPetStore Demo");

        // Move to the top page
        $(By.linkText("Enter the Store")).click();
        $(By.id("WelcomeContent")).shouldBe(text(""));

        // Move to Birds category
        $(By.cssSelector("#MainImageContent area:nth-of-type(1)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Birds"));
        $(By.linkText("Return to Main Menu")).click();

        // Move to Fish category
        $(By.cssSelector("#MainImageContent area:nth-of-type(2)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Fish"));
        $(By.linkText("Return to Main Menu")).click();

        // Move to Dogs category
        $(By.cssSelector("#MainImageContent area:nth-of-type(3)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Dogs"));
        $(By.linkText("Return to Main Menu")).click();

        // Move to Reptiles category
        $(By.cssSelector("#MainImageContent area:nth-of-type(4)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Reptiles"));
        $(By.linkText("Return to Main Menu")).click();

        // Move to Cats category
        $(By.cssSelector("#MainImageContent area:nth-of-type(5)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Cats"));
        $(By.linkText("Return to Main Menu")).click();

        // Move to Birds category
        $(By.cssSelector("#MainImageContent area:nth-of-type(6)")).click();
        $(By.cssSelector("#Catalog h2")).shouldBe(text("Birds"));
        $(By.linkText("Return to Main Menu")).click();
    }

}
